
"use strict";

let YoloResult = require('./YoloResult.js');
let YoloResultList = require('./YoloResultList.js');

module.exports = {
  YoloResult: YoloResult,
  YoloResultList: YoloResultList,
};
