;; Auto-generated. Do not edit!


(when (boundp 'yolov5::YoloResultList)
  (if (not (find-package "YOLOV5"))
    (make-package "YOLOV5"))
  (shadow 'YoloResultList (find-package "YOLOV5")))
(unless (find-package "YOLOV5::YOLORESULTLIST")
  (make-package "YOLOV5::YOLORESULTLIST"))

(in-package "ROS")
;;//! \htmlinclude YoloResultList.msg.html


(defclass yolov5::YoloResultList
  :super ros::object
  :slots (_ret ))

(defmethod yolov5::YoloResultList
  (:init
   (&key
    ((:ret __ret) ())
    )
   (send-super :init)
   (setq _ret __ret)
   self)
  (:ret
   (&rest __ret)
   (if (keywordp (car __ret))
       (send* _ret __ret)
     (progn
       (if __ret (setq _ret (car __ret)))
       _ret)))
  (:serialization-length
   ()
   (+
    ;; yolov5/YoloResult[] _ret
    (apply #'+ (send-all _ret :serialization-length)) 4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; yolov5/YoloResult[] _ret
     (write-long (length _ret) s)
     (dolist (elem _ret)
       (send elem :serialize s)
       )
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; yolov5/YoloResult[] _ret
   (let (n)
     (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4)
     (setq _ret (let (r) (dotimes (i n) (push (instance yolov5::YoloResult :init) r)) r))
     (dolist (elem- _ret)
     (send elem- :deserialize buf ptr-) (incf ptr- (send elem- :serialization-length))
     ))
   ;;
   self)
  )

(setf (get yolov5::YoloResultList :md5sum-) "9349865ade84d11e4dea577fcb41a777")
(setf (get yolov5::YoloResultList :datatype-) "yolov5/YoloResultList")
(setf (get yolov5::YoloResultList :definition-)
      "yolov5/YoloResult[] ret
================================================================================
MSG: yolov5/YoloResult
int16 cls
float32[] x
float32[] y
")



(provide :yolov5/YoloResultList "9349865ade84d11e4dea577fcb41a777")


