
"use strict";

let SetVelXRT = require('./SetVelXRT.js')
let SetVelJRT = require('./SetVelJRT.js')
let SetAccXRT = require('./SetAccXRT.js')
let GetRTControlOutputVersionList = require('./GetRTControlOutputVersionList.js')
let WriteDataRT = require('./WriteDataRT.js')
let StartRTControl = require('./StartRTControl.js')
let StopRTControl = require('./StopRTControl.js')
let ConnectRTControl = require('./ConnectRTControl.js')
let DisconnectRTControl = require('./DisconnectRTControl.js')
let GetRTControlOutputDataList = require('./GetRTControlOutputDataList.js')
let GetRTControlInputVersionList = require('./GetRTControlInputVersionList.js')
let SetRTControlInput = require('./SetRTControlInput.js')
let SetAccJRT = require('./SetAccJRT.js')
let SetRTControlOutput = require('./SetRTControlOutput.js')
let GetRTControlInputDataList = require('./GetRTControlInputDataList.js')
let ReadDataRT = require('./ReadDataRT.js')

module.exports = {
  SetVelXRT: SetVelXRT,
  SetVelJRT: SetVelJRT,
  SetAccXRT: SetAccXRT,
  GetRTControlOutputVersionList: GetRTControlOutputVersionList,
  WriteDataRT: WriteDataRT,
  StartRTControl: StartRTControl,
  StopRTControl: StopRTControl,
  ConnectRTControl: ConnectRTControl,
  DisconnectRTControl: DisconnectRTControl,
  GetRTControlOutputDataList: GetRTControlOutputDataList,
  GetRTControlInputVersionList: GetRTControlInputVersionList,
  SetRTControlInput: SetRTControlInput,
  SetAccJRT: SetAccJRT,
  SetRTControlOutput: SetRTControlOutput,
  GetRTControlInputDataList: GetRTControlInputDataList,
  ReadDataRT: ReadDataRT,
};
