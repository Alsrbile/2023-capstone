<!-- # 2023 capstone -->

# 2023 Capstone Design 

<div align=center>
	<b>식물공장 재배상 모듈용 로봇 시스템 개발 </b><br> 
	<i>2023 Seoultech Univ. Capstone Design</i> 
	<br><br>
	
</div>


<div align=center>
	<img src="https://img.shields.io/badge/Ubuntu 20.04-E95420?style=flat&logo=Ubuntu&logoColor=white"/>
	<img src="https://img.shields.io/badge/ROS Noetic-22314E?style=flat&logo=ROS&logoColor=white"/> <br>
	<img src="https://img.shields.io/badge/python-blue?style=flat&logo=python&logoColor=white"/>
	<img src="https://img.shields.io/badge/PyTorch-EE4C2C?style=flat&logo=PyTorch&logoColor=white"/>
	<img src="https://img.shields.io/badge/C++-00599C?style=flat&logo=cplusplus&logoColor=white"/>
	
</div>

## Enviroments
Doosan-Robotics collaborative robot m1013 <br>
Intel-RealSense 435i<br>
SCHUNK EGP gripper<br>

## Build
This program is implemented at Ubuntu 20.04 - ROS-Noetic

#### 0) ROS-Noetic
[http://wiki.ros.org/noetic/Installation/Ubuntu](http://wiki.ros.org/noetic/Installation/Ubuntu)<br>


Our project needs the packages written below.<br>


#### 1) Doosan-Robotics noetic-devel 
[https://github.com/doosan-robotics/doosan-robot#overview](https://github.com/doosan-robotics/doosan-robot#overview)<br>

#### 2) Intel-Realsense
[https://github.com/IntelRealSense/realsense-ros/tree/ros1-legacy](https://github.com/IntelRealSense/realsense-ros/tree/ros1-legacy)<br>

#### 3) PCL
[https://github.com/PointCloudLibrary/pcl](https://github.com/PointCloudLibrary/pcl)<br>

Cuda environment is **cuda 11.6+cudnn 8.2.0** <br>
Pytorch version is **1.13.1+cu116**

## Getting started

```bash
#### If "package {} not found" error pops up, enter this command
source ~/catkin_test/devel/setup.bash

# 1
cd catkin_test
roscore

# 2
source ~/catkin_test/devel/setup.bash
roslaunch dsr_launcher single_robot_rviz.launch model:=m1013 mode:=real host:=192.168.137.100

# 3
source ~/catkin_test/devel/setup.bash
rosrun plantfarm_test study

# 4
roslaunch realsense2_camera rs_camera.launch align_depth:=true depth_width:=640 depth_height:=480 depth_fps:=30 color_width:=640 color_height:=480 color_fps:=30

# 5
source ~/catkin_test/devel/setup.bash
rosrun yolov5 src/seg/segment/predict_copy.py

# 6
source ~/catkin_test/devel/setup.bash
rosrun plantfarm_test seperate_abnormal_node

# 7
source ~/catkin_test/devel/setup.bash
rosrun plantfarm_test register_abnormal_node

# 8
rviz

# 9
source ~/catkin_test/devel/setup.bash
rosrun plantfarm_test position.py
```

## Reference
[YOLOv7 segmentation](https://github.com/WongKinYiu/yolov7) <br>



# 23.04.24 업데이트
catkin_test/src 안에는 pcl, doosan-robot, realsense_ros가 설치되어야함 <br>
프로젝트 메인은 palntfarm_test 파일 <br>
catkin_test/src/plantfarm_test/src/seperate_abnormal.cpp : 포인트 클라우드 받아오기 <br>



<!-- 
To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/Alsrbile/2023-capstone.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/Alsrbile/2023-capstone/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.  -->
