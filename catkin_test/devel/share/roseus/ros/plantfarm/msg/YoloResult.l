;; Auto-generated. Do not edit!


(when (boundp 'plantfarm::YoloResult)
  (if (not (find-package "PLANTFARM"))
    (make-package "PLANTFARM"))
  (shadow 'YoloResult (find-package "PLANTFARM")))
(unless (find-package "PLANTFARM::YOLORESULT")
  (make-package "PLANTFARM::YOLORESULT"))

(in-package "ROS")
;;//! \htmlinclude YoloResult.msg.html


(defclass plantfarm::YoloResult
  :super ros::object
  :slots (_cls _x _y ))

(defmethod plantfarm::YoloResult
  (:init
   (&key
    ((:cls __cls) 0)
    ((:x __x) (make-array 0 :initial-element 0.0 :element-type :float))
    ((:y __y) (make-array 0 :initial-element 0.0 :element-type :float))
    )
   (send-super :init)
   (setq _cls (round __cls))
   (setq _x __x)
   (setq _y __y)
   self)
  (:cls
   (&optional __cls)
   (if __cls (setq _cls __cls)) _cls)
  (:x
   (&optional __x)
   (if __x (setq _x __x)) _x)
  (:y
   (&optional __y)
   (if __y (setq _y __y)) _y)
  (:serialization-length
   ()
   (+
    ;; int16 _cls
    2
    ;; float32[] _x
    (* 4    (length _x)) 4
    ;; float32[] _y
    (* 4    (length _y)) 4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; int16 _cls
       (write-word _cls s)
     ;; float32[] _x
     (write-long (length _x) s)
     (dotimes (i (length _x))
       (sys::poke (elt _x i) (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
       )
     ;; float32[] _y
     (write-long (length _y) s)
     (dotimes (i (length _y))
       (sys::poke (elt _y i) (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
       )
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; int16 _cls
     (setq _cls (sys::peek buf ptr- :short)) (incf ptr- 2)
   ;; float32[] _x
   (let (n)
     (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4)
     (setq _x (instantiate float-vector n))
     (dotimes (i n)
     (setf (elt _x i) (sys::peek buf ptr- :float)) (incf ptr- 4)
     ))
   ;; float32[] _y
   (let (n)
     (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4)
     (setq _y (instantiate float-vector n))
     (dotimes (i n)
     (setf (elt _y i) (sys::peek buf ptr- :float)) (incf ptr- 4)
     ))
   ;;
   self)
  )

(setf (get plantfarm::YoloResult :md5sum-) "0f0725b3ae6d75e9a523ac6866da0df5")
(setf (get plantfarm::YoloResult :datatype-) "plantfarm/YoloResult")
(setf (get plantfarm::YoloResult :definition-)
      "int16 cls
float32[] x
float32[] y
")



(provide :plantfarm/YoloResult "0f0725b3ae6d75e9a523ac6866da0df5")


