// Generated by gencpp from file plantfarm_test/tuto.msg
// DO NOT EDIT!


#ifndef PLANTFARM_TEST_MESSAGE_TUTO_H
#define PLANTFARM_TEST_MESSAGE_TUTO_H


#include <string>
#include <vector>
#include <memory>

#include <ros/types.h>
#include <ros/serialization.h>
#include <ros/builtin_message_traits.h>
#include <ros/message_operations.h>

#include <sensor_msgs/PointCloud2.h>

namespace plantfarm_test
{
template <class ContainerAllocator>
struct tuto_
{
  typedef tuto_<ContainerAllocator> Type;

  tuto_()
    : id(0)
    , cloud()  {
    }
  tuto_(const ContainerAllocator& _alloc)
    : id(0)
    , cloud(_alloc)  {
  (void)_alloc;
    }



   typedef int16_t _id_type;
  _id_type id;

   typedef  ::sensor_msgs::PointCloud2_<ContainerAllocator>  _cloud_type;
  _cloud_type cloud;





  typedef boost::shared_ptr< ::plantfarm_test::tuto_<ContainerAllocator> > Ptr;
  typedef boost::shared_ptr< ::plantfarm_test::tuto_<ContainerAllocator> const> ConstPtr;

}; // struct tuto_

typedef ::plantfarm_test::tuto_<std::allocator<void> > tuto;

typedef boost::shared_ptr< ::plantfarm_test::tuto > tutoPtr;
typedef boost::shared_ptr< ::plantfarm_test::tuto const> tutoConstPtr;

// constants requiring out of line definition



template<typename ContainerAllocator>
std::ostream& operator<<(std::ostream& s, const ::plantfarm_test::tuto_<ContainerAllocator> & v)
{
ros::message_operations::Printer< ::plantfarm_test::tuto_<ContainerAllocator> >::stream(s, "", v);
return s;
}


template<typename ContainerAllocator1, typename ContainerAllocator2>
bool operator==(const ::plantfarm_test::tuto_<ContainerAllocator1> & lhs, const ::plantfarm_test::tuto_<ContainerAllocator2> & rhs)
{
  return lhs.id == rhs.id &&
    lhs.cloud == rhs.cloud;
}

template<typename ContainerAllocator1, typename ContainerAllocator2>
bool operator!=(const ::plantfarm_test::tuto_<ContainerAllocator1> & lhs, const ::plantfarm_test::tuto_<ContainerAllocator2> & rhs)
{
  return !(lhs == rhs);
}


} // namespace plantfarm_test

namespace ros
{
namespace message_traits
{





template <class ContainerAllocator>
struct IsMessage< ::plantfarm_test::tuto_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct IsMessage< ::plantfarm_test::tuto_<ContainerAllocator> const>
  : TrueType
  { };

template <class ContainerAllocator>
struct IsFixedSize< ::plantfarm_test::tuto_<ContainerAllocator> >
  : FalseType
  { };

template <class ContainerAllocator>
struct IsFixedSize< ::plantfarm_test::tuto_<ContainerAllocator> const>
  : FalseType
  { };

template <class ContainerAllocator>
struct HasHeader< ::plantfarm_test::tuto_<ContainerAllocator> >
  : FalseType
  { };

template <class ContainerAllocator>
struct HasHeader< ::plantfarm_test::tuto_<ContainerAllocator> const>
  : FalseType
  { };


template<class ContainerAllocator>
struct MD5Sum< ::plantfarm_test::tuto_<ContainerAllocator> >
{
  static const char* value()
  {
    return "705b14f0c1f3e1a19c7b9b4799b8c3d6";
  }

  static const char* value(const ::plantfarm_test::tuto_<ContainerAllocator>&) { return value(); }
  static const uint64_t static_value1 = 0x705b14f0c1f3e1a1ULL;
  static const uint64_t static_value2 = 0x9c7b9b4799b8c3d6ULL;
};

template<class ContainerAllocator>
struct DataType< ::plantfarm_test::tuto_<ContainerAllocator> >
{
  static const char* value()
  {
    return "plantfarm_test/tuto";
  }

  static const char* value(const ::plantfarm_test::tuto_<ContainerAllocator>&) { return value(); }
};

template<class ContainerAllocator>
struct Definition< ::plantfarm_test::tuto_<ContainerAllocator> >
{
  static const char* value()
  {
    return "int16 id\n"
"sensor_msgs/PointCloud2 cloud\n"
"================================================================================\n"
"MSG: sensor_msgs/PointCloud2\n"
"# This message holds a collection of N-dimensional points, which may\n"
"# contain additional information such as normals, intensity, etc. The\n"
"# point data is stored as a binary blob, its layout described by the\n"
"# contents of the \"fields\" array.\n"
"\n"
"# The point cloud data may be organized 2d (image-like) or 1d\n"
"# (unordered). Point clouds organized as 2d images may be produced by\n"
"# camera depth sensors such as stereo or time-of-flight.\n"
"\n"
"# Time of sensor data acquisition, and the coordinate frame ID (for 3d\n"
"# points).\n"
"Header header\n"
"\n"
"# 2D structure of the point cloud. If the cloud is unordered, height is\n"
"# 1 and width is the length of the point cloud.\n"
"uint32 height\n"
"uint32 width\n"
"\n"
"# Describes the channels and their layout in the binary data blob.\n"
"PointField[] fields\n"
"\n"
"bool    is_bigendian # Is this data bigendian?\n"
"uint32  point_step   # Length of a point in bytes\n"
"uint32  row_step     # Length of a row in bytes\n"
"uint8[] data         # Actual point data, size is (row_step*height)\n"
"\n"
"bool is_dense        # True if there are no invalid points\n"
"\n"
"================================================================================\n"
"MSG: std_msgs/Header\n"
"# Standard metadata for higher-level stamped data types.\n"
"# This is generally used to communicate timestamped data \n"
"# in a particular coordinate frame.\n"
"# \n"
"# sequence ID: consecutively increasing ID \n"
"uint32 seq\n"
"#Two-integer timestamp that is expressed as:\n"
"# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')\n"
"# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')\n"
"# time-handling sugar is provided by the client library\n"
"time stamp\n"
"#Frame this data is associated with\n"
"string frame_id\n"
"\n"
"================================================================================\n"
"MSG: sensor_msgs/PointField\n"
"# This message holds the description of one point entry in the\n"
"# PointCloud2 message format.\n"
"uint8 INT8    = 1\n"
"uint8 UINT8   = 2\n"
"uint8 INT16   = 3\n"
"uint8 UINT16  = 4\n"
"uint8 INT32   = 5\n"
"uint8 UINT32  = 6\n"
"uint8 FLOAT32 = 7\n"
"uint8 FLOAT64 = 8\n"
"\n"
"string name      # Name of field\n"
"uint32 offset    # Offset from start of point struct\n"
"uint8  datatype  # Datatype enumeration, see above\n"
"uint32 count     # How many elements in the field\n"
;
  }

  static const char* value(const ::plantfarm_test::tuto_<ContainerAllocator>&) { return value(); }
};

} // namespace message_traits
} // namespace ros

namespace ros
{
namespace serialization
{

  template<class ContainerAllocator> struct Serializer< ::plantfarm_test::tuto_<ContainerAllocator> >
  {
    template<typename Stream, typename T> inline static void allInOne(Stream& stream, T m)
    {
      stream.next(m.id);
      stream.next(m.cloud);
    }

    ROS_DECLARE_ALLINONE_SERIALIZER
  }; // struct tuto_

} // namespace serialization
} // namespace ros

namespace ros
{
namespace message_operations
{

template<class ContainerAllocator>
struct Printer< ::plantfarm_test::tuto_<ContainerAllocator> >
{
  template<typename Stream> static void stream(Stream& s, const std::string& indent, const ::plantfarm_test::tuto_<ContainerAllocator>& v)
  {
    s << indent << "id: ";
    Printer<int16_t>::stream(s, indent + "  ", v.id);
    s << indent << "cloud: ";
    s << std::endl;
    Printer< ::sensor_msgs::PointCloud2_<ContainerAllocator> >::stream(s, indent + "  ", v.cloud);
  }
};

} // namespace message_operations
} // namespace ros

#endif // PLANTFARM_TEST_MESSAGE_TUTO_H
