
(cl:in-package :asdf)

(defsystem "plantfarm_test-msg"
  :depends-on (:roslisp-msg-protocol :roslisp-utils :sensor_msgs-msg
)
  :components ((:file "_package")
    (:file "YoloResult" :depends-on ("_package_YoloResult"))
    (:file "_package_YoloResult" :depends-on ("_package"))
    (:file "YoloResultList" :depends-on ("_package_YoloResultList"))
    (:file "_package_YoloResultList" :depends-on ("_package"))
    (:file "tuto" :depends-on ("_package_tuto"))
    (:file "_package_tuto" :depends-on ("_package"))
  ))