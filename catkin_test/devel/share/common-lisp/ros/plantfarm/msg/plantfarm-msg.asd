
(cl:in-package :asdf)

(defsystem "plantfarm-msg"
  :depends-on (:roslisp-msg-protocol :roslisp-utils :sensor_msgs-msg
)
  :components ((:file "_package")
    (:file "PreprocessedPointcloud" :depends-on ("_package_PreprocessedPointcloud"))
    (:file "_package_PreprocessedPointcloud" :depends-on ("_package"))
    (:file "YoloResult" :depends-on ("_package_YoloResult"))
    (:file "_package_YoloResult" :depends-on ("_package"))
    (:file "YoloResultList" :depends-on ("_package_YoloResultList"))
    (:file "_package_YoloResultList" :depends-on ("_package"))
  ))