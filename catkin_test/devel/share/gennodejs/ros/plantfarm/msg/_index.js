
"use strict";

let YoloResult = require('./YoloResult.js');
let PreprocessedPointcloud = require('./PreprocessedPointcloud.js');
let YoloResultList = require('./YoloResultList.js');

module.exports = {
  YoloResult: YoloResult,
  PreprocessedPointcloud: PreprocessedPointcloud,
  YoloResultList: YoloResultList,
};
