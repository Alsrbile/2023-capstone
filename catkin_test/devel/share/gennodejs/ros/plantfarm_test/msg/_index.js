
"use strict";

let YoloResult = require('./YoloResult.js');
let tuto = require('./tuto.js');
let YoloResultList = require('./YoloResultList.js');

module.exports = {
  YoloResult: YoloResult,
  tuto: tuto,
  YoloResultList: YoloResultList,
};
