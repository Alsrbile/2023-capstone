// Auto-generated. Do not edit!

// (in-package plantfarm_test.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;
let YoloResult = require('./YoloResult.js');

//-----------------------------------------------------------

class YoloResultList {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.ret = null;
    }
    else {
      if (initObj.hasOwnProperty('ret')) {
        this.ret = initObj.ret
      }
      else {
        this.ret = [];
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type YoloResultList
    // Serialize message field [ret]
    // Serialize the length for message field [ret]
    bufferOffset = _serializer.uint32(obj.ret.length, buffer, bufferOffset);
    obj.ret.forEach((val) => {
      bufferOffset = YoloResult.serialize(val, buffer, bufferOffset);
    });
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type YoloResultList
    let len;
    let data = new YoloResultList(null);
    // Deserialize message field [ret]
    // Deserialize array length for message field [ret]
    len = _deserializer.uint32(buffer, bufferOffset);
    data.ret = new Array(len);
    for (let i = 0; i < len; ++i) {
      data.ret[i] = YoloResult.deserialize(buffer, bufferOffset)
    }
    return data;
  }

  static getMessageSize(object) {
    let length = 0;
    object.ret.forEach((val) => {
      length += YoloResult.getMessageSize(val);
    });
    return length + 4;
  }

  static datatype() {
    // Returns string type for a message object
    return 'plantfarm_test/YoloResultList';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '9349865ade84d11e4dea577fcb41a777';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    plantfarm_test/YoloResult[] ret
    
    ================================================================================
    MSG: plantfarm_test/YoloResult
    int16 cls
    float32[] x
    float32[] y
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new YoloResultList(null);
    if (msg.ret !== undefined) {
      resolved.ret = new Array(msg.ret.length);
      for (let i = 0; i < resolved.ret.length; ++i) {
        resolved.ret[i] = YoloResult.Resolve(msg.ret[i]);
      }
    }
    else {
      resolved.ret = []
    }

    return resolved;
    }
};

module.exports = YoloResultList;
