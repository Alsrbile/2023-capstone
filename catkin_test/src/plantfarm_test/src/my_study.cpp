#include <ros/ros.h>

using namespace std;

class PointCloud;

int main(int argc, char** argv)
{
    // <topic의 주소>
    std::string depth_topic         = "/camera/aligned_depth_to_color/image_raw";
    std::string inrinsic_topic      = "/camera/aligned_depth_to_color/camera_info";

    std::string pointcloud_topic    = "/mgsong";

    ros::init(argc, argv, "mgSong_study");

}

class PointCloud
{
    public:
        PointCloud(string depth_topic, string intrinsics_topic, string pointcloud_topic)
        {
            ROS_INFO("Depth 이미지 pointcloud로 변환 start");

            depth_sub = nh.subscribe(depth_topic, 10, this);
                
        }

    void depthCallback(const sensor_msgs::)
    
    private:
        ros::NodeHandle nh;     // ros의 node 선언
        
        ros::Subscriber depth_sub;
        ros::Subscriber intrinsics_sub;
        ros::Publisher pointcloud_sub;

        

}