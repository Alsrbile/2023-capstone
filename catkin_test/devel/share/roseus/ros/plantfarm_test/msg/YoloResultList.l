;; Auto-generated. Do not edit!


(when (boundp 'plantfarm_test::YoloResultList)
  (if (not (find-package "PLANTFARM_TEST"))
    (make-package "PLANTFARM_TEST"))
  (shadow 'YoloResultList (find-package "PLANTFARM_TEST")))
(unless (find-package "PLANTFARM_TEST::YOLORESULTLIST")
  (make-package "PLANTFARM_TEST::YOLORESULTLIST"))

(in-package "ROS")
;;//! \htmlinclude YoloResultList.msg.html


(defclass plantfarm_test::YoloResultList
  :super ros::object
  :slots (_ret ))

(defmethod plantfarm_test::YoloResultList
  (:init
   (&key
    ((:ret __ret) ())
    )
   (send-super :init)
   (setq _ret __ret)
   self)
  (:ret
   (&rest __ret)
   (if (keywordp (car __ret))
       (send* _ret __ret)
     (progn
       (if __ret (setq _ret (car __ret)))
       _ret)))
  (:serialization-length
   ()
   (+
    ;; plantfarm_test/YoloResult[] _ret
    (apply #'+ (send-all _ret :serialization-length)) 4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; plantfarm_test/YoloResult[] _ret
     (write-long (length _ret) s)
     (dolist (elem _ret)
       (send elem :serialize s)
       )
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; plantfarm_test/YoloResult[] _ret
   (let (n)
     (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4)
     (setq _ret (let (r) (dotimes (i n) (push (instance plantfarm_test::YoloResult :init) r)) r))
     (dolist (elem- _ret)
     (send elem- :deserialize buf ptr-) (incf ptr- (send elem- :serialization-length))
     ))
   ;;
   self)
  )

(setf (get plantfarm_test::YoloResultList :md5sum-) "9349865ade84d11e4dea577fcb41a777")
(setf (get plantfarm_test::YoloResultList :datatype-) "plantfarm_test/YoloResultList")
(setf (get plantfarm_test::YoloResultList :definition-)
      "plantfarm_test/YoloResult[] ret

================================================================================
MSG: plantfarm_test/YoloResult
int16 cls
float32[] x
float32[] y
")



(provide :plantfarm_test/YoloResultList "9349865ade84d11e4dea577fcb41a777")


