
(cl:in-package :asdf)

(defsystem "yolov5-msg"
  :depends-on (:roslisp-msg-protocol :roslisp-utils )
  :components ((:file "_package")
    (:file "YoloResult" :depends-on ("_package_YoloResult"))
    (:file "_package_YoloResult" :depends-on ("_package"))
    (:file "YoloResultList" :depends-on ("_package_YoloResultList"))
    (:file "_package_YoloResultList" :depends-on ("_package"))
  ))