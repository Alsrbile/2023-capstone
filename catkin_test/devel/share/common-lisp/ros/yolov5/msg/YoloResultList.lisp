; Auto-generated. Do not edit!


(cl:in-package yolov5-msg)


;//! \htmlinclude YoloResultList.msg.html

(cl:defclass <YoloResultList> (roslisp-msg-protocol:ros-message)
  ((ret
    :reader ret
    :initarg :ret
    :type (cl:vector yolov5-msg:YoloResult)
   :initform (cl:make-array 0 :element-type 'yolov5-msg:YoloResult :initial-element (cl:make-instance 'yolov5-msg:YoloResult))))
)

(cl:defclass YoloResultList (<YoloResultList>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <YoloResultList>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'YoloResultList)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name yolov5-msg:<YoloResultList> is deprecated: use yolov5-msg:YoloResultList instead.")))

(cl:ensure-generic-function 'ret-val :lambda-list '(m))
(cl:defmethod ret-val ((m <YoloResultList>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader yolov5-msg:ret-val is deprecated.  Use yolov5-msg:ret instead.")
  (ret m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <YoloResultList>) ostream)
  "Serializes a message object of type '<YoloResultList>"
  (cl:let ((__ros_arr_len (cl:length (cl:slot-value msg 'ret))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_arr_len) ostream))
  (cl:map cl:nil #'(cl:lambda (ele) (roslisp-msg-protocol:serialize ele ostream))
   (cl:slot-value msg 'ret))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <YoloResultList>) istream)
  "Deserializes a message object of type '<YoloResultList>"
  (cl:let ((__ros_arr_len 0))
    (cl:setf (cl:ldb (cl:byte 8 0) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) __ros_arr_len) (cl:read-byte istream))
  (cl:setf (cl:slot-value msg 'ret) (cl:make-array __ros_arr_len))
  (cl:let ((vals (cl:slot-value msg 'ret)))
    (cl:dotimes (i __ros_arr_len)
    (cl:setf (cl:aref vals i) (cl:make-instance 'yolov5-msg:YoloResult))
  (roslisp-msg-protocol:deserialize (cl:aref vals i) istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<YoloResultList>)))
  "Returns string type for a message object of type '<YoloResultList>"
  "yolov5/YoloResultList")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'YoloResultList)))
  "Returns string type for a message object of type 'YoloResultList"
  "yolov5/YoloResultList")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<YoloResultList>)))
  "Returns md5sum for a message object of type '<YoloResultList>"
  "9349865ade84d11e4dea577fcb41a777")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'YoloResultList)))
  "Returns md5sum for a message object of type 'YoloResultList"
  "9349865ade84d11e4dea577fcb41a777")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<YoloResultList>)))
  "Returns full string definition for message of type '<YoloResultList>"
  (cl:format cl:nil "yolov5/YoloResult[] ret~%================================================================================~%MSG: yolov5/YoloResult~%int16 cls~%float32[] x~%float32[] y~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'YoloResultList)))
  "Returns full string definition for message of type 'YoloResultList"
  (cl:format cl:nil "yolov5/YoloResult[] ret~%================================================================================~%MSG: yolov5/YoloResult~%int16 cls~%float32[] x~%float32[] y~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <YoloResultList>))
  (cl:+ 0
     4 (cl:reduce #'cl:+ (cl:slot-value msg 'ret) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ (roslisp-msg-protocol:serialization-length ele))))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <YoloResultList>))
  "Converts a ROS message object to a list"
  (cl:list 'YoloResultList
    (cl:cons ':ret (ret msg))
))
