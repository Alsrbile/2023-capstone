# generated from genmsg/cmake/pkg-genmsg.cmake.em

message(STATUS "plantfarm: 3 messages, 0 services")

set(MSG_I_FLAGS "-Iplantfarm:/home/alsrbile/catkin_test/src/plantfarm/msg;-Istd_msgs:/opt/ros/noetic/share/std_msgs/cmake/../msg;-Isensor_msgs:/opt/ros/noetic/share/sensor_msgs/cmake/../msg;-Igeometry_msgs:/opt/ros/noetic/share/geometry_msgs/cmake/../msg")

# Find all generators
find_package(gencpp REQUIRED)
find_package(geneus REQUIRED)
find_package(genlisp REQUIRED)
find_package(gennodejs REQUIRED)
find_package(genpy REQUIRED)

add_custom_target(plantfarm_generate_messages ALL)

# verify that message/service dependencies have not changed since configure



get_filename_component(_filename "/home/alsrbile/catkin_test/src/plantfarm/msg/YoloResult.msg" NAME_WE)
add_custom_target(_plantfarm_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "plantfarm" "/home/alsrbile/catkin_test/src/plantfarm/msg/YoloResult.msg" ""
)

get_filename_component(_filename "/home/alsrbile/catkin_test/src/plantfarm/msg/YoloResultList.msg" NAME_WE)
add_custom_target(_plantfarm_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "plantfarm" "/home/alsrbile/catkin_test/src/plantfarm/msg/YoloResultList.msg" "plantfarm/YoloResult"
)

get_filename_component(_filename "/home/alsrbile/catkin_test/src/plantfarm/msg/PreprocessedPointcloud.msg" NAME_WE)
add_custom_target(_plantfarm_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "plantfarm" "/home/alsrbile/catkin_test/src/plantfarm/msg/PreprocessedPointcloud.msg" "sensor_msgs/PointCloud2:sensor_msgs/PointField:std_msgs/Header"
)

#
#  langs = gencpp;geneus;genlisp;gennodejs;genpy
#

### Section generating for lang: gencpp
### Generating Messages
_generate_msg_cpp(plantfarm
  "/home/alsrbile/catkin_test/src/plantfarm/msg/YoloResult.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/plantfarm
)
_generate_msg_cpp(plantfarm
  "/home/alsrbile/catkin_test/src/plantfarm/msg/YoloResultList.msg"
  "${MSG_I_FLAGS}"
  "/home/alsrbile/catkin_test/src/plantfarm/msg/YoloResult.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/plantfarm
)
_generate_msg_cpp(plantfarm
  "/home/alsrbile/catkin_test/src/plantfarm/msg/PreprocessedPointcloud.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/noetic/share/sensor_msgs/cmake/../msg/PointCloud2.msg;/opt/ros/noetic/share/sensor_msgs/cmake/../msg/PointField.msg;/opt/ros/noetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/plantfarm
)

### Generating Services

### Generating Module File
_generate_module_cpp(plantfarm
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/plantfarm
  "${ALL_GEN_OUTPUT_FILES_cpp}"
)

add_custom_target(plantfarm_generate_messages_cpp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_cpp}
)
add_dependencies(plantfarm_generate_messages plantfarm_generate_messages_cpp)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/alsrbile/catkin_test/src/plantfarm/msg/YoloResult.msg" NAME_WE)
add_dependencies(plantfarm_generate_messages_cpp _plantfarm_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/alsrbile/catkin_test/src/plantfarm/msg/YoloResultList.msg" NAME_WE)
add_dependencies(plantfarm_generate_messages_cpp _plantfarm_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/alsrbile/catkin_test/src/plantfarm/msg/PreprocessedPointcloud.msg" NAME_WE)
add_dependencies(plantfarm_generate_messages_cpp _plantfarm_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(plantfarm_gencpp)
add_dependencies(plantfarm_gencpp plantfarm_generate_messages_cpp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS plantfarm_generate_messages_cpp)

### Section generating for lang: geneus
### Generating Messages
_generate_msg_eus(plantfarm
  "/home/alsrbile/catkin_test/src/plantfarm/msg/YoloResult.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/plantfarm
)
_generate_msg_eus(plantfarm
  "/home/alsrbile/catkin_test/src/plantfarm/msg/YoloResultList.msg"
  "${MSG_I_FLAGS}"
  "/home/alsrbile/catkin_test/src/plantfarm/msg/YoloResult.msg"
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/plantfarm
)
_generate_msg_eus(plantfarm
  "/home/alsrbile/catkin_test/src/plantfarm/msg/PreprocessedPointcloud.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/noetic/share/sensor_msgs/cmake/../msg/PointCloud2.msg;/opt/ros/noetic/share/sensor_msgs/cmake/../msg/PointField.msg;/opt/ros/noetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/plantfarm
)

### Generating Services

### Generating Module File
_generate_module_eus(plantfarm
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/plantfarm
  "${ALL_GEN_OUTPUT_FILES_eus}"
)

add_custom_target(plantfarm_generate_messages_eus
  DEPENDS ${ALL_GEN_OUTPUT_FILES_eus}
)
add_dependencies(plantfarm_generate_messages plantfarm_generate_messages_eus)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/alsrbile/catkin_test/src/plantfarm/msg/YoloResult.msg" NAME_WE)
add_dependencies(plantfarm_generate_messages_eus _plantfarm_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/alsrbile/catkin_test/src/plantfarm/msg/YoloResultList.msg" NAME_WE)
add_dependencies(plantfarm_generate_messages_eus _plantfarm_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/alsrbile/catkin_test/src/plantfarm/msg/PreprocessedPointcloud.msg" NAME_WE)
add_dependencies(plantfarm_generate_messages_eus _plantfarm_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(plantfarm_geneus)
add_dependencies(plantfarm_geneus plantfarm_generate_messages_eus)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS plantfarm_generate_messages_eus)

### Section generating for lang: genlisp
### Generating Messages
_generate_msg_lisp(plantfarm
  "/home/alsrbile/catkin_test/src/plantfarm/msg/YoloResult.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/plantfarm
)
_generate_msg_lisp(plantfarm
  "/home/alsrbile/catkin_test/src/plantfarm/msg/YoloResultList.msg"
  "${MSG_I_FLAGS}"
  "/home/alsrbile/catkin_test/src/plantfarm/msg/YoloResult.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/plantfarm
)
_generate_msg_lisp(plantfarm
  "/home/alsrbile/catkin_test/src/plantfarm/msg/PreprocessedPointcloud.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/noetic/share/sensor_msgs/cmake/../msg/PointCloud2.msg;/opt/ros/noetic/share/sensor_msgs/cmake/../msg/PointField.msg;/opt/ros/noetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/plantfarm
)

### Generating Services

### Generating Module File
_generate_module_lisp(plantfarm
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/plantfarm
  "${ALL_GEN_OUTPUT_FILES_lisp}"
)

add_custom_target(plantfarm_generate_messages_lisp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_lisp}
)
add_dependencies(plantfarm_generate_messages plantfarm_generate_messages_lisp)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/alsrbile/catkin_test/src/plantfarm/msg/YoloResult.msg" NAME_WE)
add_dependencies(plantfarm_generate_messages_lisp _plantfarm_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/alsrbile/catkin_test/src/plantfarm/msg/YoloResultList.msg" NAME_WE)
add_dependencies(plantfarm_generate_messages_lisp _plantfarm_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/alsrbile/catkin_test/src/plantfarm/msg/PreprocessedPointcloud.msg" NAME_WE)
add_dependencies(plantfarm_generate_messages_lisp _plantfarm_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(plantfarm_genlisp)
add_dependencies(plantfarm_genlisp plantfarm_generate_messages_lisp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS plantfarm_generate_messages_lisp)

### Section generating for lang: gennodejs
### Generating Messages
_generate_msg_nodejs(plantfarm
  "/home/alsrbile/catkin_test/src/plantfarm/msg/YoloResult.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/plantfarm
)
_generate_msg_nodejs(plantfarm
  "/home/alsrbile/catkin_test/src/plantfarm/msg/YoloResultList.msg"
  "${MSG_I_FLAGS}"
  "/home/alsrbile/catkin_test/src/plantfarm/msg/YoloResult.msg"
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/plantfarm
)
_generate_msg_nodejs(plantfarm
  "/home/alsrbile/catkin_test/src/plantfarm/msg/PreprocessedPointcloud.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/noetic/share/sensor_msgs/cmake/../msg/PointCloud2.msg;/opt/ros/noetic/share/sensor_msgs/cmake/../msg/PointField.msg;/opt/ros/noetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/plantfarm
)

### Generating Services

### Generating Module File
_generate_module_nodejs(plantfarm
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/plantfarm
  "${ALL_GEN_OUTPUT_FILES_nodejs}"
)

add_custom_target(plantfarm_generate_messages_nodejs
  DEPENDS ${ALL_GEN_OUTPUT_FILES_nodejs}
)
add_dependencies(plantfarm_generate_messages plantfarm_generate_messages_nodejs)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/alsrbile/catkin_test/src/plantfarm/msg/YoloResult.msg" NAME_WE)
add_dependencies(plantfarm_generate_messages_nodejs _plantfarm_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/alsrbile/catkin_test/src/plantfarm/msg/YoloResultList.msg" NAME_WE)
add_dependencies(plantfarm_generate_messages_nodejs _plantfarm_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/alsrbile/catkin_test/src/plantfarm/msg/PreprocessedPointcloud.msg" NAME_WE)
add_dependencies(plantfarm_generate_messages_nodejs _plantfarm_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(plantfarm_gennodejs)
add_dependencies(plantfarm_gennodejs plantfarm_generate_messages_nodejs)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS plantfarm_generate_messages_nodejs)

### Section generating for lang: genpy
### Generating Messages
_generate_msg_py(plantfarm
  "/home/alsrbile/catkin_test/src/plantfarm/msg/YoloResult.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/plantfarm
)
_generate_msg_py(plantfarm
  "/home/alsrbile/catkin_test/src/plantfarm/msg/YoloResultList.msg"
  "${MSG_I_FLAGS}"
  "/home/alsrbile/catkin_test/src/plantfarm/msg/YoloResult.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/plantfarm
)
_generate_msg_py(plantfarm
  "/home/alsrbile/catkin_test/src/plantfarm/msg/PreprocessedPointcloud.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/noetic/share/sensor_msgs/cmake/../msg/PointCloud2.msg;/opt/ros/noetic/share/sensor_msgs/cmake/../msg/PointField.msg;/opt/ros/noetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/plantfarm
)

### Generating Services

### Generating Module File
_generate_module_py(plantfarm
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/plantfarm
  "${ALL_GEN_OUTPUT_FILES_py}"
)

add_custom_target(plantfarm_generate_messages_py
  DEPENDS ${ALL_GEN_OUTPUT_FILES_py}
)
add_dependencies(plantfarm_generate_messages plantfarm_generate_messages_py)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/alsrbile/catkin_test/src/plantfarm/msg/YoloResult.msg" NAME_WE)
add_dependencies(plantfarm_generate_messages_py _plantfarm_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/alsrbile/catkin_test/src/plantfarm/msg/YoloResultList.msg" NAME_WE)
add_dependencies(plantfarm_generate_messages_py _plantfarm_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/alsrbile/catkin_test/src/plantfarm/msg/PreprocessedPointcloud.msg" NAME_WE)
add_dependencies(plantfarm_generate_messages_py _plantfarm_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(plantfarm_genpy)
add_dependencies(plantfarm_genpy plantfarm_generate_messages_py)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS plantfarm_generate_messages_py)



if(gencpp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/plantfarm)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/plantfarm
    DESTINATION ${gencpp_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_cpp)
  add_dependencies(plantfarm_generate_messages_cpp std_msgs_generate_messages_cpp)
endif()
if(TARGET sensor_msgs_generate_messages_cpp)
  add_dependencies(plantfarm_generate_messages_cpp sensor_msgs_generate_messages_cpp)
endif()

if(geneus_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/plantfarm)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/plantfarm
    DESTINATION ${geneus_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_eus)
  add_dependencies(plantfarm_generate_messages_eus std_msgs_generate_messages_eus)
endif()
if(TARGET sensor_msgs_generate_messages_eus)
  add_dependencies(plantfarm_generate_messages_eus sensor_msgs_generate_messages_eus)
endif()

if(genlisp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/plantfarm)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/plantfarm
    DESTINATION ${genlisp_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_lisp)
  add_dependencies(plantfarm_generate_messages_lisp std_msgs_generate_messages_lisp)
endif()
if(TARGET sensor_msgs_generate_messages_lisp)
  add_dependencies(plantfarm_generate_messages_lisp sensor_msgs_generate_messages_lisp)
endif()

if(gennodejs_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/plantfarm)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/plantfarm
    DESTINATION ${gennodejs_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_nodejs)
  add_dependencies(plantfarm_generate_messages_nodejs std_msgs_generate_messages_nodejs)
endif()
if(TARGET sensor_msgs_generate_messages_nodejs)
  add_dependencies(plantfarm_generate_messages_nodejs sensor_msgs_generate_messages_nodejs)
endif()

if(genpy_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/plantfarm)
  install(CODE "execute_process(COMMAND \"/usr/bin/python3\" -m compileall \"${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/plantfarm\")")
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/plantfarm
    DESTINATION ${genpy_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_py)
  add_dependencies(plantfarm_generate_messages_py std_msgs_generate_messages_py)
endif()
if(TARGET sensor_msgs_generate_messages_py)
  add_dependencies(plantfarm_generate_messages_py sensor_msgs_generate_messages_py)
endif()
